
function getData(type,id){
    let url = 'fileservlet';
    if(type != undefined && id != undefined)
        url = `fileservlet?type=${type}&id=${id}`;
    console.log(url);
    $.getJSON(url,{type:type,id:id},(data)=>{
       appendDataToHTML(data) 
    });
}

function appendDataToHTML(json){
    document.getElementById("backlinks_row").innerHTML = `<td><span class='bold'> Backlinks: </span></td>`; //reset
        for (var i = 0; i < json.backlinks.length; i++) {
            let backlink = json.backlinks[i];
            let fname = backlink.name.split('\\').pop(); // only filename, no path
            let type = 'backlink';
            document.getElementById("backlinks_row").innerHTML += `<td><a class='plain' href='#' id='${i}' onclick=getData('backlink',this.id);>${fname} ></a></td>`;
        }
       

        document.getElementById("curr_path").innerHTML = json.cur_dir.name;
        document.getElementById("curr_size").innerHTML = json.cur_dir.size+' B';
        document.getElementById("curr_total_files").innerHTML = json.cur_dir.total_files;
        document.getElementById("curr_total_folders").innerHTML = json.cur_dir.total_folders;

        //files list html
        let row_data = `<tr><th>File Name </th><th> Type </th><th> Size </th><th> Percentage </th></tr>`;
        for (var i = 0; i < json.files.length; i++) {
            let file = json.files[i];
            let fname = file.name.split('\\').pop(); // only filename, no path
            let percent = Math.round(file.percent * 1000000) / 1000000;
            let type=file.ftype;
            if(type!='file' && type!='junction_file')
                row_data += `<tr><td>* <a href='#' id='${i}' onclick=getData('file',this.id)>${fname}</a></td><td>${file.ftype}</td><td>${file.size} B</td><td>${percent} %</td></tr>`;
            else
                row_data += `<tr><td>- ${fname}</td><td>${file.ftype}</td><td>${file.size} B</td><td>${percent} %</td></tr>`;
        }
        document.getElementById("files").innerHTML = row_data;
        console.log(json);
        
        console.log(json.root.name);
        console.log(json.cur_dir.name);
        
        if(json.root.name !== json.cur_dir.name){
            document.getElementById("links").style.display = "";
        }else{
            document.getElementById("links").style.display = "none";
        }
}


getData();


