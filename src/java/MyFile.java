
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author arvind-pt3044
 */
public class MyFile {

    int type;
    long size;
    List<MyFile> childs; //transient , so it doesnt shows up when serialized. i.e wehen converted from Java Object to Json
    String name;
    final MyFile parent;
    double percent;
    boolean isLink;
    private final String ftype;
    long total_files;
    private int total_folders;

    public MyFile(int type, long size, String name, MyFile parent, boolean isLink) {
        this.type = type; // 0 is file , 1 is folder
        this.size = size;
        this.isLink = isLink;
        this.percent = 0.0;
        this.name = name;
        this.parent = parent;
        this.childs = new ArrayList<>();
        this.ftype = this.getType();
        this.total_files = 0;
        this.total_folders = 0;
    }
    
    
    
    //calculates self size recursively
    public long calculateMySize() {
        
        //if type is file, parent's total files incremented by  +1
        if (this.type == 0) {
            this.parent.total_files += 1;
        }

        if (this.size >= 0) //folders have -1 size initially and files have size > -1
        {
//            System.out.println("Calculating size for: "+this.name);
            return this.size;
        }

        long mysize = 0;
        for (MyFile f : childs) {
            f.calculateMySize();
            if ((this.isLink && f.isLink) || (this.isLink || !f.isLink)) {
                mysize += f.size;
                total_files += f.total_files;
            }
        }
        
        this.size = mysize; //new size
        
        
        //if type is folder && parent is folder and not link
        if ((this.type == 1 && this.parent != null && !this.isLink)) {
            this.parent.total_folders += 1;
            this.parent.total_folders += this.total_folders;
        }
        
        //after parent's size is done being calculated, calculate childs percentage
        for (int i = 0; i < this.childs.size(); i++) {
            if (this.size != 0) {
                this.childs.get(i).percent = (this.childs.get(i).size / (double) this.size) * 100;
            }
        }
        
        //in end return self's size
        return this.size;
    }

    public void addChild(MyFile f) {
//        System.out.println("Adding "+f+" to "+this.name);
        this.childs.add(f);
    }

    private static class CustomComp implements Comparator<MyFile> {

        @Override
        public int compare(MyFile o1, MyFile o2) {
            return (int) (o2.size - o1.size);
        }
    }

    public void sortChild() {
        this.childs.sort(new CustomComp());
    }

    @Override
    public String toString() {
        return this.name + " "; 
    }

    public String getType() {
        if (this.isLink && this.type == 0) {
            return "junction_file";
        } else if (this.isLink && this.type == 1) {
            return "junction_folder";
        }

        if (this.type == 0) {
            return "file";
        } else {
            return "folder";
        }

    }

    static boolean isLink(Path p) {
        try {
            return !p.equals(p.toRealPath()); //if real path doesnt matches current path, then link
        } catch (IOException e) {
            return false;
        }
    }

    public String toJson() {
        String name = this.name.replace("\\", "\\\\"); //sanitize

        String json = "{";
        json += "\"size\":" + this.size + ",\n";
        json += "\"name\":" + "\"" + name + "\",\n";
        json += "\"percent\":" + this.percent + ",\n";
        json += "\"isLink\":" + this.isLink + ",\n";
        json += "\"ftype\":\"" + this.ftype + "\",\n";
        json += "\"total_files\":" + this.total_files + ",\n";
        json += "\"total_folders\":" + this.total_folders + "\n";
        json += "}";

        return json;
    }
}
