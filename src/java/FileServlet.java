/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author arvind-pt3044
 */

public class FileServlet extends HttpServlet {

    private List<MyFile> flist;
    private List<MyFile> backlink_list;
    private File root_f;
    private MyFile root;
    private MyFile curr;
    private String root_dir;
    private String root_name;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        //set content type of the response
        res.setContentType("application/json;charset=UTF-8");
        String id = req.getParameter("id");
        String type = req.getParameter("type");
        
        if (id != null && type != null) {
//            if (!id.equals("undefined") && !type.equals("undefined")) {
                int index = Integer.parseInt(id);
                if (type.equals("file")) {

                    //to go back
                    if (index == -1) {
                        curr = curr.parent;
                        backlink_list.subList(backlink_list.size() - 1, backlink_list.size()).clear();
                    } //to go to root
                    else if (index == -2) {
                        curr = root;
                        backlink_list.clear();
                        backlink_list.add(curr);
                    } //to go to next child
                    else if (index >= 0) {
                        curr = curr.childs.get(index);
                        backlink_list.add(curr);
                    }
                } //to go to any of the backlinks
                else if (type.equals("backlink")) {
                    curr = backlink_list.get(index);
                    backlink_list.subList(index + 1, backlink_list.size()).clear(); //remove all backlinks after the position
                }

//            }
        } //if no parameter supplied, reset cur dir to root
        else {
            curr = root;
            flist = curr.childs;
            backlink_list.clear();
            backlink_list.add(curr);
        }

        curr.sortChild(); //sort child of curr
        //list of files
        flist = curr.childs;

        try (PrintWriter pw = res.getWriter()) {
            Map<String, Object> jsonObj = new HashMap<>();

            jsonObj.put("root", root);
            jsonObj.put("cur_dir", curr);
            jsonObj.put("files", flist);
            jsonObj.put("backlinks", backlink_list);
            
            String jsonData = Util.toJson(jsonObj);
//            System.out.println("data: "+jsonData);
            pw.write(jsonData);
        

        }
    }

    @Override
    public void init() throws ServletException {
        super.init();

        root_dir = "C:\\Program Files";
        root = new MyFile(1, -1, root_dir, null, false);
        root_name = root.name; //for backup
        
    
        //create the tree and find time taken
        long start = System.currentTimeMillis();
//        Util.createTree2(root); //create Tree
        Util.createTreeMultiThreaded(root);
        long end = System.currentTimeMillis();
        System.out.println("Time taken: " + ((end - start)/1000.0)+" s");
        flist = new ArrayList<MyFile>();
        backlink_list = new ArrayList<MyFile>();
        System.out.println("Init called!");
    }

}
