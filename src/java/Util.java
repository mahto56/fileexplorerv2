
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author arvind-pt3044
 */
public class Util {

    public static void main(String[] s) {
        long start = System.currentTimeMillis();
        String root_dir = "C:\\Program Files";
        MyFile root = new MyFile(1, -1, root_dir, null, false);
        FileTreeBuilderAction fa = new FileTreeBuilderAction(root);
        ForkJoinPool.commonPool().invoke(fa);
        long stop = System.currentTimeMillis();
        System.out.println(stop - start);
    }

    static void createTree(MyFile root) {

        File f = new File(root.name);
        File[] root_files = f.listFiles();
        if (root_files != null) {
            for (File cf : root_files) {
                if (cf.isFile()) {
                    if (MyFile.isLink(cf.toPath())) {
                        root.addChild(new MyFile(0, cf.length(), cf.getAbsolutePath(), root, true));
                    } else {
                        root.addChild(new MyFile(0, cf.length(), cf.getAbsolutePath(), root, false));
                    }
                } else {
                    MyFile mf;
                    if (MyFile.isLink(cf.toPath())) {
                        mf = new MyFile(1, -1, cf.getAbsolutePath(), root, true);
                    } else {
                        mf = new MyFile(1, -1, cf.getAbsolutePath(), root, false);
                    }
                    root.addChild(mf);
                    createTree(mf);
                }
            }
        }

        root.calculateMySize(); //calculate the size initially

    }

    //using directory stream
    static void createTree2(MyFile root) {
        DirectoryStream<Path> stream;
        try {
            stream = Files.newDirectoryStream(Paths.get(root.name));
            for (Path path : stream) {
                if (!Files.isDirectory(path)) {
                    if (MyFile.isLink(path)) {
                        root.addChild(new MyFile(0, Files.size(path), path.toString(), root, true));
                    } else {
                        root.addChild(new MyFile(0, Files.size(path), path.toString(), root, false));
                    }
                } else {
                    MyFile mf;
                    if (MyFile.isLink(path)) {
                        mf = new MyFile(1, -1, path.toString(), root, true);
                    } else {
                        mf = new MyFile(1, -1, path.toString(), root, false);
                    }
                    root.addChild(mf);
                    createTree2(mf);
                }
            }
            stream.close();
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }

        //after building the tree calculate the size
        root.calculateMySize();

    }
    
    
    
    //multithreaded tree builder
    static void createTreeMultiThreaded(MyFile root) {
        FileTreeBuilderAction fa = new FileTreeBuilderAction(root);
        ForkJoinPool.commonPool().invoke(fa);
    }

    static String toJson(MyFile f) {
        return f.toJson();
    }

    static String toJson(List<MyFile> flist) {
        String json = "[";
        for (int i = 0; i < flist.size(); i++) {
            json += flist.get(i).toJson();

            if (i != flist.size() - 1) {
                json += ",\n";
            } else {
                json += "]";
            }
        }
        return json;
    }

    static String toJson(Map<String, Object> fileMap) {
        String json = "{";
        int i = 0;
        for (Map.Entry<String, Object> entry : fileMap.entrySet()) {
            json += "\"" + entry.getKey() + "\":";
            json += Util.toJson(entry.getValue());
            if (i != fileMap.size() - 1) {
                json += ",\n";
            } else {
                json += "}";
            }
            i++;
        }
        return json;
    }

    static String toJson(Object obj) {
        String json = "";
        if (obj instanceof MyFile) {
            json = Util.toJson((MyFile) obj);
        } else if (obj instanceof List) {
            json += Util.toJson((List) obj);
        } else {
            throw new IllegalArgumentException("Unknown object type");
        }
        return json;

    }

    
    //using listFiles()
    private static class FileTreeBuilderAction extends RecursiveAction {

        private final MyFile root;

        public FileTreeBuilderAction(MyFile root) {
            this.root = root;
        }

        @Override
        protected void compute() {
            List<FileTreeBuilderAction> actionlist = new ArrayList<>(); // a list for keeping track of child threads
            File f = new File(root.name);//root file
            File[] root_childs = f.listFiles();
            if (root_childs != null) {
                for (File cf : root_childs) {
                    if (cf.isFile()) {
                        if (MyFile.isLink(cf.toPath())) {
                            root.addChild(new MyFile(0, cf.length(), cf.getAbsolutePath(), root, true));
                        } else {
                            root.addChild(new MyFile(0, cf.length(), cf.getAbsolutePath(), root, false));
                        }
                        if(root.parent!=null)
                            root.parent.size+=root.size;
                    } else {
                        MyFile mf;
                        if (MyFile.isLink(cf.toPath())) {
                            mf = new MyFile(1, -1, cf.getAbsolutePath(), root, true);
                        } else {
                            mf = new MyFile(1, -1, cf.getAbsolutePath(), root, false);
                        }
                        root.addChild(mf);
                        FileTreeBuilderAction childAction = new FileTreeBuilderAction(mf);
                        actionlist.add(childAction);
                        childAction.fork(); //start child thread in root thread
                    }

                }
            }

            actionlist.forEach( fa -> fa.join());  // wait for all its child to complete
           
            root.calculateMySize(); //only after all child's size are done , calculate self size

        }

    }

    
    
    //using streams 
    private static class FileTreeBuilderAction2 extends RecursiveAction {

        private final MyFile root;

        public FileTreeBuilderAction2(MyFile root) {
            this.root = root;
        }

        @Override
        protected void compute() {
            List<FileTreeBuilderAction2> actionlist = new ArrayList<>(); // a list for keeping track of child threads
            DirectoryStream<Path> stream;
            try {
                stream = Files.newDirectoryStream(Paths.get(root.name));
                for (Path path : stream) {
                    if (!Files.isDirectory(path)) {
                        if (MyFile.isLink(path)) {
                            root.addChild(new MyFile(0, Files.size(path), path.toString(), root, true));
                        } else {
                            root.addChild(new MyFile(0, Files.size(path), path.toString(), root, false));
                        }
                    } else {
                        MyFile mf;
                        if (MyFile.isLink(path)) {
                            mf = new MyFile(1, -1, path.toString(), root, true);
                        } else {
                            mf = new MyFile(1, -1, path.toString(), root, false);
                        }
                        root.addChild(mf);
                        FileTreeBuilderAction2 childAction = new FileTreeBuilderAction2(mf);
                        actionlist.add(childAction);
                        childAction.fork(); //start child thread in root thread
                    }
                }
                stream.close();
            } catch (IOException ex) {
                Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            }

            //after building the tree calculate the size
           // root.calculateMySize();

            actionlist.forEach((fa) -> {
                fa.join(); // wait for all its child to complete
            });

            root.calculateMySize(); //only after all child's size are done , calculate self size

        }

    }
}
